options(encoding = "UTF-8")
library(vein) # vein
library(sf) # spatial data
library(cptcity) # 7120 colour palettes
library(ggplot2) # plots
library(eixport) # WRF Chem
library(data.table) # blasting speed
library(fem)
sessionInfo()

dir.create("post")
dir.create("post/grids")
dir.create("wrf")


# 0 read emissions
data("brazil")
df <- brazil[, c(
    "Name", "Sector", "Lat", "Long",
    "NOx-mean(Gg/ano)",
    "CO-mean(Gg/ano)",
    "SOx-mean(Gg/ano)",
    "MP-mean(Gg/ano)",
    "TOC-mean(Gg/ano)",
    "CO2-mean(Gg/ano)"
)]

for (i in 5:10) df[[i]] <- df[[i]] * units::set_units(1, "Gg/h") / 365 / 24
for (i in 5:10) df[[i]] <- units::set_units(df[[i]], "g/h")
names(df)[5:10] <- c(
    "NOx", "CO", "SO2", # asumi que SOx e SO2
    "PM", "TOC", "CO2"
)
df$NO <- 0.9 * df$NOx
df$NO2 <- 0.1 * df$NOx

df <- as.data.frame(df)
df$Long <- as.numeric(df$Long)
df <- df[!is.na(df$Long), ]
summary(df)

df <- st_as_sf(
    x = df,
    coords = c("Long", "Lat"),
    crs = 4326
)

df
# grade
g <- wrf_grid("../wrf/wrfinput_d03")
# Number of lat points 152
# Number of lon points 160
dff <- st_crop(df, g)
saveRDS(dff, "post/gemis.rds")
rm(list = ls())
gc()



# poluentes sem especiar ####
g <- wrf_grid("../wrf/wrfinput_d03")
dff <- readRDS("post/gemis.rds")
polu <- c("NO", "NO2", "CO", "SO2", "CO2", "PM")
source("scripts/post_gases.R")
rm(list = ls())
gc()

# postmech ####
dff <- readRDS("post/gemis.rds")
sector <- "Sector" # column including industrial sector for each code, see ?fem::spec_industry
tfs <- readRDS("../config/tfs.rds")
g <- wrf_grid("../wrf/wrfinput_d03")
pol <- c("NO", "NO2", "CO", "SO2", "CO2")
mol <- c(14 + 16, 14 + 16 * 2, 12 + 16, 32 + 16 * 2, 12 + 16 * 2)
aer <- "pmneu2" # pmiag, pmneu
HC <- "TOC" # column of dff to speciate gases
PM <- "PM" # column of dff to speciate PM
code_list <- list(
    "Petroleum_refining" = "01_petroleum", # sector from dff = code from fem::spec_industry
    "Portland_cement" = "02_portland_cement",
    "Paper_and_cellulose" = "03_paper_cellulose",
    "Natural_gas_TPP" = "05_natural_gas",
    "Diesel_TPP" = "07_fuel_oil"
)
verbose <- TRUE
mechs <- c("CBMZ", "S99", "RADM2", "CB05")
mechs <- "CBMZ"
for (z in seq_along(mechs)) {
    mech <- mechs[z]
    source("scripts/mech_industrial.R")
}
rm(list = ls())
gc()



# WRF CHEM ####
mechs <- c("CBMZ", "S99", "RADM2", "CB05")
mechs <- "CBMZ"
for (z in seq_along(mechs)) {
    print(mechs[z])
    mech <- mechs[z]
    language <- "english" # english spanish
    tfs <- readRDS("../config/tfs.rds")
    net <- readRDS("../network/net.rds")
    dir_mech <- paste0("post/", mech)
    cols <- 152
    rows <- 160
    wrf_times <- 24 # ?
    dir_wrfinput <- "../wrf"
    dir_wrfchemi <- "wrf"
    domain <- 3
    hours <- 0
    io_style_emissions <- 1 # 2 for all the hours, 1 for two emission files 0-12z
    n_aero <- 15 # number of PM species
    rotate <- "cols" # see ?GriddedEmissionsArray
    # radm2 and racm2 would not include ETOH. Then, ETOH emissions (mol) is added in HC3
    # if mech is CB05, CBMZ or MOZT1, ETOH is renamed to C2H5OH
    # CB05 works con ETOH, si se usa opt2 en wrf emis_opt$ecb05_opt2[order(emis_opt$ecb05_opt2)]
    source("../scripts/wrf.R", encoding = "UTF-8")
}
mechs <- c("CBMZ", "S99", "RADM2", "CB05")
mechs <- "CBMZ"
for (z in seq_along(mechs)) {
    file.rename(from = paste0("wrf/", mechs[z], "/wrfchemi_00z_d03"), to = paste0("wrf/", mechs[z], "/industry_wrfchemi_00z_d03"))
    file.rename(from = paste0("wrf/", mechs[z], "/wrfchemi_12z_d03"), to = paste0("wrf/", mechs[z], "/industry_wrfchemi_12z_d03"))
}

# merge wrf ####
language <- "english" # english spanish
cols <- 152
rows <- 160
wrf_times <- 24 # ?
dir_wrfinput <- "../wrf"
dir_wrfchemi <- "wrf"
domain <- 3
hours <- 0
wrf_industry_0 <- "wrf/CBMZ/industry_wrfchemi_00z_d03"
wrf_industry_1 <- "wrf/CBMZ/industry_wrfchemi_12z_d03"
wrf_veh_0 <- "../wrf/CBMZ/veicular_wrfchemi_00z_d03"
wrf_veh_1 <- "../wrf/CBMZ/veicular_wrfchemi_12z_d03"
source("scripts/wrf_industry.R", encoding = "UTF-8")
