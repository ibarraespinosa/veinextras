dir.create(paste0("post/", mech), showWarnings = FALSE)

# grades para especiar ####
m <- Emissions(matrix(data = dff[[HC]], nrow = nrow(dff), ncol = nrow(tfs)), time = "h") # transforming in 24 hours
nx <- names(m)
m$Sector <- dff$Sector
dt <- st_sf(m, geometry = dff$geometry)

dt[[sector]] <- gsub(pattern = " ", replacement = "_", x = dt[[sector]])
toc <- split(dt[, paste0("V", 1:nrow(tfs))], dt[[sector]])

lx <- lapply(
  seq_along(toc),
  function(i) {
    st_set_geometry(emis_grid(spobj = toc[[i]], g, type = "point", sr = 3857), NULL)
  }
)
names(lx) <- names(toc)

lxx <- lapply(seq_along(code_list), function(k) {
  dfx <- spec_industry(
    x = lx[[names(code_list)[k]]],
    code = code_list[[k]]
  )
  setDF(dfx)
  dfx$x.id <- NULL
  names(dfx) <- c(paste0("V", 1:nrow(tfs)), "pol")
  dfx$id <- rep(1:nrow(dt), length(unique(dfx$pol)))
  head(dfx)

  voc01 <- suppressWarnings(emis_chem2(
    df = dfx,
    mech = mech,
    nx = nx,
    na.rm = TRUE
  ))
})

voc <- rbindlist(lxx)


dfvoc <- voc[,
  lapply(.SD, sum, na.rm = T),
  .SDcols = nx,
  by = .(id, group)
]

voc <- split(dfvoc, dfvoc$group)

names(voc) <- paste0("E_", toupper(names(voc)))
# saving VOC ####
voc[is.na(voc)] <- 0
names(voc)

for (i in seq_along(names(voc))) {
  g_x <- g
  g_x <- merge(g, voc[[names(voc)[i]]], by = "id", all = T)
  g_x <- st_sf(g_x, geometry = g$geometry)
  g_x$group <- NULL
  saveRDS(g_x, file = paste0("post/", mech, "/", names(voc)[i], ".rds"))
  if (verbose) print(paste0("post/", mech, "/", names(voc)[i], ".rds"))
}


# other gases ####
mm_x <- units::set_units(mol, "g/mol") # mm: massa molar

for (i in seq_along(pol)) {
  print(pol[i])
  m <- Emissions(matrix(
    data = dff[[pol[i]]],
    nrow = nrow(dff),
    ncol = 24
  ),
  time = "h"
  ) # transforming in 24 hours
  for (j in seq_along(m)) m[[j]] <- m[[j]] / mm_x[i]
  dt <- st_sf(m, geometry = dff$geometry)
  dfg <- emis_grid(spobj = dt, g, type = "point", sr = 3857)
  saveRDS(dfg, paste0("post/", mech, "/E_", pol[i], ".rds"))
  if (verbose) print(paste0("post/", mech, "/E_", pol[i], ".rds"))
}


# PM
aer <- "pmneu2" # pmiag, pmneu

m <- Emissions(matrix(
  data = dff[["PM"]],
  nrow = nrow(dff),
  ncol = 24
),
time = "h"
) # transforming in 24 hours
dt <- st_sf(m, geometry = dff$geometry)
dfg <- emis_grid(spobj = dt, g, type = "point", sr = 3857)

gPM1 <- speciate(x = dfg, spec = aer, list = T)

for (i in 1:length(names(gPM1))) {
  gPMx <- st_sf(gPM1[[i]], geometry = g$geometry)
  saveRDS(gPMx, paste0("post/", mech, "/", toupper(names(gPM1))[i], ".rds"))
  if (verbose) paste0("post/", mech, "/", toupper(names(gPM1))[i], ".rds")
}